<?php

/**
 * @file
 * Delete file functions.
 */

/**
 * Redirect after delete files.
 */
function javali_import_media_files_delete_all_redirect_submit(&$form_submit) {
  $get_folder = filter_input(INPUT_GET, 'folder');
  $get_extensions = filter_input(INPUT_GET, 'extensions');
  drupal_goto('admin/config/media/import-media-files/delete-all/confirm', array(
    'query' => array(
      'folder' => $get_folder,
      'extensions' => $get_extensions,
    ),
  )
  );
}

/**
 * Form to delete all files.
 */
function javali_import_media_files_delete_all_files_form($form, &$form_state) {
  $get_folder = filter_input(INPUT_GET, 'folder');
  $get_extensions = filter_input(INPUT_GET, 'extensions');
  $folder = urldecode($get_folder);
  $extensions = urldecode($get_extensions);
  $question = t('Do you really want <b>delete</b> all files?');
  $path = array(
    'path' => 'admin/config/media/import-media-files',
    'query' => array(
      array(
        'folder' => $get_folder,
        'extensions' => $get_extensions,
      ),
    ),
  );

  $description = t("Warning, this is the last step before form gets submitted") . '<br><br><b>' . t('Files to delete:') . '</b>' . '<br>';

  $wrapper = file_stream_wrapper_get_instance_by_uri($folder);

  $realpath = $wrapper->realpath();
  $get_folder_comp = substr($folder, -1) == '/' ? $folder : $folder . '/';
  $ext = !empty($extensions) ? '/.*\.(' . str_replace(',', '|', preg_replace('/\s+/', '', $extensions)) . ')*$/' : '/.*\.*$/';
  $files = array();

  $direc = file_scan_directory($realpath, $ext);
  foreach ($direc as $key => $file) {
    $check = javali_import_media_files_check_if_file_exists($get_folder_comp . basename($file->filename));
    if (empty($check) && is_file($file->uri)) {
      $files[$key]['file'] = basename($file->filename);
      $files[$key]['realpath'] = $file->uri;
      $files2[] = basename($file->filename);
    }
  }
  $form['hidden_files'] = array(
    '#type' => 'hidden',
    '#value' => $files2,
  );
  if (!empty($files)) {
    foreach ($files as $key => $file) {
      $description .= $file['file'] . '<br>';
    }
  }

  return confirm_form($form, $question, $path, $description);
}

/**
 * Form to delete all files submit.
 */
function javali_import_media_files_delete_all_files_form_submit($form, &$form_state) {
  $get_folder = filter_input(INPUT_GET, 'folder');
  $get_extensions = filter_input(INPUT_GET, 'extensions');
  // dbug($form_state); exit();
  if (!empty($form_state['values']['hidden_files'])) {
    foreach ($form_state['values']['hidden_files'] as $unsaved_file) {
      unlink($unsaved_file);
    }
    drupal_set_message(t('Files sucessefully deleted'));
  }
  drupal_goto('admin/config/media/import-media-files', array('query' => array('folder' => $get_folder, 'extensions' => $get_extensions)));
}

/**
 * Redirect selected files to delete.
 */
function javali_import_media_files_delete_selected_redirect_submit($form, &$form_submit) {
  $get_folder = filter_input(INPUT_GET, 'folder');
  $get_extensions = filter_input(INPUT_GET, 'extensions');

  $new_form_state = array();
  // List path arguments that usually go into form_id_2 constructor from path2.
  $new_form_state['build_info']['args'] = array('folder' => $get_folder, 'extensions' => $get_extensions);
  $new_form_state['method'] = 'post';
  // List values you want to pass.
  $new_form_state['values'] = array();
  // dbug($form_submit); exit();
  foreach ($form_submit['values']['files_select'] as $key => $selected) {
    // if($selected != 0){.
    if (!is_numeric($form_submit['values']['files_select'][$key])) {
      $new_form_state['values'][$form_submit['values']['files_select'][$key]] = $form_submit['values']['files_select'][$key];
    }
  }
  // dbug($new_form_state); exit();
  $_SESSION['batch_form_submit'] = $new_form_state;
  drupal_goto('admin/config/media/import-media-files/delete-selected/confirm', array('query' => array('folder' => $get_folder, 'extensions' => $get_extensions)));
}

/**
 * Confirm files to delete.
 */
function javali_import_media_files_confirm_delete_selected_files_form($form, &$form_state) {
  // dbug($_SESSION['batch_form_submit']); exit();
  $get_folder = filter_input(INPUT_GET, 'folder');
  $get_extensions = filter_input(INPUT_GET, 'extensions');
  $folder = urldecode($get_folder);
  $question = t('Do you really want upload all files?');
  $path = array(
    'path' => 'admin/config/media/import-media-files',
    'query' => array(
      array(
        'folder' => $get_folder,
        'extensions' => $get_extensions,
      ),
    ),
  );

  $description = t("Warning, this is the last step before form gets deleted") . '<br><br><b>' . t('Files to delete:') . '</b>' . '<br>';

  $wrapper = file_stream_wrapper_get_instance_by_uri($folder);
  // dbug($folder); exit();
  $realpath = $wrapper->realpath();
  $get_folder_comp = substr($folder, -1) == '/' ? $folder : $folder . '/';
  $get_realfolder_comp = substr($realpath, -1) == '/' ? $realpath : $realpath . '/';
  $files = array();

  $only_files = $_SESSION['batch_form_submit']['values'];
  // dbug($only_files); exit();
  $files2 = array();
  foreach ($only_files as $key => $file) {
    $check = javali_import_media_files_check_if_file_exists($get_folder_comp . basename($file));
    if (empty($check)) {
      $files[$key]['file'] = basename($file);
      $files[$key]['realpath'] = $file;
      $files2[] = $get_realfolder_comp . $file;
    }
  }
  $form['hidden_files'] = array(
    '#type' => 'hidden',
    '#value' => $files2,
  );
  // dbug($files2);.
  if (!empty($files)) {
    foreach ($files as $file2) {
      $description .= $file2['file'] . '<br>';
    }
  }
  return confirm_form($form, $question, $path, $description);
}

/**
 * Confirm files to delete.
 */
function javali_import_media_files_confirm_delete_selected_files_form_submit($form, &$form_state) {
  $get_folder = filter_input(INPUT_GET, 'folder');
  $get_extensions = filter_input(INPUT_GET, 'extensions');
  // dbug($form_state['values']['hidden_files']); exit();
  if (!empty($form_state['values']['hidden_files'])) {
    foreach ($form_state['values']['hidden_files'] as $unsaved_file) {
      unlink($unsaved_file);
    }
    drupal_set_message(t('Files sucessefully deleted'));
  }

  drupal_goto('admin/config/media/import-media-files', array('query' => array('folder' => $get_folder, 'extensions' => $get_extensions)));
}

/**
 * Confirm single file to delete.
 */
function javali_import_media_files_confirm_delete_single_files_form($form, &$form_state) {

  $get_folder = filter_input(INPUT_GET, 'folder');
  $get_extensions = filter_input(INPUT_GET, 'extensions');
  $get_files = filter_input(INPUT_GET, 'file');
  $question = t('Do you really want delete this file?');
  $path = array(
    'path' => 'admin/config/media/import-media-files',
    'query' => array(
      array(
        'folder' => $get_folder,
        'extensions' => $get_extensions,
      ),
    ),
  );

  $description = t("Warning, this is the last step before form gets submitted") . '<br><br><b>' . t('Files to delete:') . '</b>' . '<br>';

  $form['hidden_files'] = array(
    '#type' => 'hidden',
    '#value' => $get_files,
  );

  $description .= $get_files . '<br>';

  return confirm_form($form, $question, $path, $description);
}

/**
 * Confirm single files to delete.
 */
function javali_import_media_files_confirm_delete_single_files_form_submit($form, &$form_state) {
  $get_folder = filter_input(INPUT_GET, 'folder');
  $get_extensions = filter_input(INPUT_GET, 'extensions');
  $folder = urldecode($get_folder);

  $wrapper = file_stream_wrapper_get_instance_by_uri($folder);
  // dbug($folder); exit();
  $realpath = $wrapper->realpath();
  $get_realfolder_comp = substr($realpath, -1) == '/' ? $realpath : $realpath . '/';

  // dbug($form_state['values']['hidden_files']); exit();
  if (!empty($form_state['values']['hidden_files'])) {
    unlink($get_realfolder_comp . $form_state['values']['hidden_files']);
    drupal_set_message(t('File sucessefully deleted'));
  }

  drupal_goto('admin/config/media/import-media-files', array('query' => array('folder' => $get_folder, 'extensions' => $get_extensions)));
}
