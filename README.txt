INTRODUCTION
----------------------------------------------------------------------------------
This module allow users to insert or delete unattached files to Drupal 
file system.

REQUIREMENTS
----------------------------------------------------------------------------------
No need special requirements

INSTALLATION
---------------------------------------------------------------------------------
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------------------------------------------------------------------------
Copy the files you want to import to you public:// site folder
Go to "admin/config/media/import-media-files"
Select items to import
